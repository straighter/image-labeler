import argparse
import matplotlib.pyplot as plt
import os
from PIL import Image
# This script is nothing special, it just puts images into folders using an interactive tool using keyboard to label them.
# It currently only works for a specific use-case with single digit (or character) labels.

# Make a directory at a path with an optional name
def make_dir(path, name = ''):
    path = os.path.abspath(os.path.join(path, name))

    if not os.path.exists(path):
        try:
            os.makedirs(path)
        except Exception as e:
            raise e
 
class ImageLabeler:
    def __init__(self, image_path, save_path, labels):
        self.labels = labels
        self.image_path = image_path
        self.save_path = save_path
        self.history = self.get_history()
        
        # Make dirs for all labels
        for label in labels:
            make_dirs(save_path, label)

        self.images = self.get_images()
        self.loop()

    # Check if history and set if available
    def get_history(self):
        if not os.path.exists(os.path.join(self.save_path, '.history')):
            return None

        with open(os.path.join(self.save_path, '.history'), 'r') as f:
            history = f.read()
            return int(history)
    
    # Get and load images and load from history if available
    def get_images(self):
        if not os.path.exists(self.image_path):
            exit()

        images = []

        for file in os.listdir(self.image_path):
            if file.endswith('.jpg') or file.endswith('.jpg'):
                images.append({ 'path': os.path.join(self.image_path, file), 'filename': file})
        if self.history:
            return images[self.history:]
        
        return images
    
    # Label a specific image
    def label_image(self, image, label):
        if not os.path.exists(self.save_path):
            exit()
        
        try:
            # Move image to label directory
            os.rename(image.get('path'),
                os.path.join(self.save_path, label, image.get('filename')))
        except Exception as e:
            raise e

    # On keypress in label (currently only single char labels work)
    def keypress(self, event, image):
        if event.key in self.labels:
            self.label_image(image, event.key)
        elif event.key is 'x':
            exit()
    
    # Main loop that keeps on going until all images are labeled
    def loop(self):
        # Allow interactivity
        plt.ion()
        
        # Loop through all images
        for i, image in enumerate(self.images):
            # Open image and plot
            img = Image.open(image.get('path'))
            fig = plt.figure()
            plt.imshow(img)
            
            # Hook up keypress event to plot            
            fig.canvas.mpl_connect('key_press_event', lambda event: self.keypress(event, image))
            
            plt.waitforbuttonpress()

            plt.close()



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Image path')
    parser.add_argument('imagePath', metavar='imagePath', type=str)
    parser.add_argument('savePath', metavar='savePath', type=str)

    args = parser.parse_args()
    
    # Currently used for just numbered labels
    ImageLabeler(args.imagePath, args.savePath, ['1', '2'])

    
